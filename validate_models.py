import os
import pickle
import numpy as np
from scipy.stats import t as t_dist
from torch.autograd import Variable
import torch
import sys
import torchvision.utils as vutils
from utils import *
from torch import nn
import argparse
import torchvision.datasets as dset


parser = argparse.ArgumentParser()
parser.add_argument('--model_path',default='/media/songbird/Data/mdgan_output/', required=False, help='path to models')
parser.add_argument('--data_path',default='', required=True, help='path to dataset')
parser.add_argument('--run',default='', required=True, help='name of run')
parser.add_argument('--max_num',type=int,default=-1, required=False, help='max number of batches to validate on')
parser.add_argument('--save_samples',default=10, required=False, help='number of sample songs to reconstruct')


def im_loader_no_phase(path):
    im = np.load(path)
    im = random_crop(im,width=16)
    im = transform(im)
    return im

def transform(im):
    im = from_polar(im)
    im,phase = lc.magphase(im)
    im = np.log1p(im)
    return im

def inverse_transform(im):
    random_phase = im.copy()
    np.random.shuffle(random_phase)
    p = phase_restore((np.exp(im) - 1), random_phase, 256, N=200)
    return (np.exp(im)-1)*p


def main():
    args = parser.parse_args()

    runs = os.listdir(args.model_path)
    run = args.run

    for i in range(len(runs)):
        if run in runs[i]:
            path = os.path.join(args.model_path,runs[i])
            break

    with open(os.path.join(path, 'opt.pkl'), 'rb') as f:
        opt = pickle.load(f)
    print("testing: ",path)

    os.makedirs(join(path,'validation'),exist_ok=True)

    ngpu=opt.ngpu
    nz=opt.nz
    ngf=opt.ngf
    nc=opt.nc
    ndf=opt.ndf
    if torch.cuda.is_available() and not opt.cuda:
        print("WARNING: You have a CUDA device, so you should probably run with --cuda")

    dataset_train = dset.ImageFolder(root=args.data_path,loader=im_loader_no_phase)
    dataloader_train = torch.utils.data.DataLoader(dataset_train, batch_size=opt.batchSize,
                                             shuffle=True, num_workers=int(opt.workers))
    dataset_val = dset.ImageFolder(root=args.data_path+'_val',loader=im_loader_no_phase)
    dataloader_val = torch.utils.data.DataLoader(dataset_val, batch_size=opt.batchSize,
                                             shuffle=True, num_workers=int(opt.workers))
    train_mean = []
    validation_mean = []
    train_std = []
    validation_std = []
    if args.max_num == -1:
        max_num = min(len(dataloader_train), len(dataloader_val))
    else:
        max_num = args.max_num

    if opt.distance_fun=='L1':
        print('Using L1 loss')
        criterion_dist = nn.L1Loss()
    else:
        print('Using L2 loss')
        criterion_dist = nn.MSELoss()
    lowest_val_epoch = 0
    lowest_mean = np.inf
    for iteration in range(opt.niter):
        net_G_file = os.path.join(path,'netG_epoch_%d.pth'%(iteration))
        net_E_file = os.path.join(path,'netE_epoch_%d.pth'%(iteration))
        if not os.path.isfile(net_G_file):
            continue
        try:
            from networks_1d import _netG, _netE, _netD, weights_init, GANLoss
            netG = _netG(ngpu, nz, ngf, nc)
            netG.apply(weights_init)
            netG.load_state_dict(torch.load(net_G_file))
            netE = _netE(ngpu, nz, ngf, nc)
            netE.apply(weights_init)
            netE.load_state_dict(torch.load(net_E_file))
        except:
            from networks_audio_nophase import _netG, _netE, _netD, weights_init, GANLoss
            netG = _netG(ngpu, nz, ngf, nc)
            netG.apply(weights_init)
            netG.load_state_dict(torch.load(net_G_file))
            netE = _netE(ngpu, nz, ngf, nc)
            netE.apply(weights_init)
            netE.load_state_dict(torch.load(net_E_file))

        netG.cuda()
        netE.cuda()
        train_loss = []
        validation_loss = []
        input = torch.FloatTensor(opt.batchSize, nc, opt.imageH, opt.imageW)
        input = input.cuda()
        input = Variable(input)
        criterion_dist.cuda()

        print("available train set batches: ",len(dataloader_train))
        print("available validation set batches: ",len(dataloader_val))
        for i, data in enumerate(dataloader_train):
            print("Model [%d/%d] -- Training set batch [%d/%d]"%(iteration,opt.niter,i,max_num))
            if i>=max_num:
                break
            real_cpu, _ = data
            real_cpu.cuda()
            batch_size = real_cpu.size(0)
            rr = torch.Tensor(batch_size,nc,opt.imageH,opt.imageW)
            input.data.resize_(rr.size()).copy_(real_cpu)
            encoding = netE(input)
            netG.mode(reconstruction=True)
            reconstruction = netG(encoding)
            train_loss.append(criterion_dist(reconstruction, input).data[0])

        for i, data in enumerate(dataloader_val):
            print("Model [%d/%d] -- Validation set batch [%d/%d]"%(iteration,opt.niter,i,max_num))
            if i>=max_num:
                break
            real_cpu, _ = data
            real_cpu.cuda()
            batch_size = real_cpu.size(0)
            rr = torch.Tensor(batch_size,nc,opt.imageH,opt.imageW)
            input.data.resize_(rr.size()).copy_(real_cpu)
            encoding = netE(input)
            netG.mode(reconstruction=True)
            reconstruction = netG(encoding)
            validation_loss.append(criterion_dist(reconstruction, input).data[0])

        train_mean.append(np.mean(train_loss))
        validation_mean.append(np.mean(validation_loss))
        train_std.append(np.std(train_loss))
        validation_std.append(np.std(validation_loss))
        if np.mean(validation_loss)<lowest_mean:
            lowest_mean=np.mean(validation_loss)
            lowest_val_epoch = iteration

    out_shape = [opt.imageH, opt.imageW]
    net_G_file = os.path.join(path, 'netG_epoch_%d.pth' % (lowest_val_epoch))
    net_E_file = os.path.join(path, 'netE_epoch_%d.pth' % (lowest_val_epoch))
    try:
        from networks_1d import _netG, _netE, _netD, weights_init, GANLoss
        netG = _netG(ngpu, nz, ngf, nc)
        netG.apply(weights_init)
        netG.load_state_dict(torch.load(net_G_file))
        netE = _netE(ngpu, nz, ngf, nc)
        netE.apply(weights_init)
        netE.load_state_dict(torch.load(net_E_file))
    except:
        from networks_audio_nophase import _netG, _netE, _netD, weights_init, GANLoss
        netG = _netG(ngpu, nz, ngf, nc)
        netG.apply(weights_init)
        netG.load_state_dict(torch.load(net_G_file))
        netE = _netE(ngpu, nz, ngf, nc)
        netE.apply(weights_init)
        netE.load_state_dict(torch.load(net_E_file))

    netG.cuda()
    netE.cuda()

    #Save some samples of the latest model with validation data
    for i in range(int(args.save_samples)):
        sample_file = get_random_sample(args.data_path+'_val')
        sample = np.load(sample_file)
        save_audio_sample(lc.istft(inverse_transform(transform(sample))),
                          '%s/input_test_sample_%d.wav' % (join(path,'validation'), i), 16000)

        sample_segments = segment_image(sample, width=opt.imageW)
        sample_segments = [transform(k) for k in sample_segments]
        sample_batches, num_segments = to_batches(sample_segments, opt.batchSize)
        netG.mode(reconstruction=True)
        reconstructed_samples = []
        cnt = 0
        for j in range(len(sample_batches)):
            input.data.copy_(torch.from_numpy(sample_batches[j]))
            encoding = netE(input)
            reconstruction = netG(encoding)
            for k in range(reconstruction.data.cpu().numpy().shape[0]):
                if cnt < num_segments:
                    reconstructed_samples.append(
                        inverse_transform(reconstruction.data[k, :, :, :].cpu().numpy().reshape(out_shape)))
                    cnt += 1

        reconstructed_samples = np.concatenate(reconstructed_samples, axis=1)
        reconstructed_audio = lc.istft(reconstructed_samples)
        save_audio_sample(reconstructed_audio, '%s/reconstructed_test_sample_%d.wav' % (join(path,'validation'), i), 16000)

    with open(join(path,'validation','validation.csv'),'w') as f:
        f.write("train_mean,train_std,validation_mean,validation_std\n")
        for i in range(len(train_mean)):
            f.write("%.4f,%.4f,%.4f,%.4f\n"%(train_mean[i],train_std[i],validation_mean[i],validation_std[i]))
    open(join(path,"validation","best_model_epoch_%d.txt"%(lowest_val_epoch)),'w').close()


if __name__=='__main__':
    main()
