# Generating Birdsong with Generative Adversarial Networks

### On this page you can find links to audio clips to comapre results from different models

- For a detailed report on this project see BirdsongGenerationWithGANs.pdf
- See requirements.txt for package versions
- The current implementation uses .npy files to store the data. This requires a small fix in the source code of 
    /torchvision/datasets/folder.py. Add '.npy' to the list of allowed extensions in line 7.
    Or maybe a better option is to use another format for the input data, or just not use the pytorch dataloader tool.



### 1. Reconstruction
#### MD-GAN
##### n_z = 8
1. [input](https://clyp.it/5eflmvtf) [output](https://clyp.it/zkurmc0v)
2. [input](https://clyp.it/tm3xnetq) [output](https://clyp.it/rtckmg13)
3. [input](https://clyp.it/53pvw3md) [output](https://clyp.it/jus1ivpj)

##### n_z = 16
1. [input](https://clyp.it/o02pqwos) [output](https://clyp.it/2215ftdm)
2. [input](https://clyp.it/qtlkul1h) [output](https://clyp.it/2tpusc0g)
3. [input](https://clyp.it/wpd2a2ng) [output](https://clyp.it/b0ce2vro)

##### n_z = 32
1. [input](https://clyp.it/3qekflck) [output](https://clyp.it/nghtd0z3)
2. [input](https://clyp.it/pvgtft4a) [output](https://clyp.it/tso0hf02)
3. [input](https://clyp.it/f5ykpsxr) [output](https://clyp.it/yavhps0j)

##### n_z = 64
1. [input](https://clyp.it/avgxervd) [output](https://clyp.it/ywhwyf1n)
2. [input](https://clyp.it/kusqremj) [output](https://clyp.it/gzl2zoqr)
3. [input](https://clyp.it/qekrcfxs) [output](https://clyp.it/zd3weyza)

##### n_z = 128
1. [input](https://clyp.it/5a5wht5y) [output](https://clyp.it/k1cz1t2l)
2. [input](https://clyp.it/ve2xxiih) [output](https://clyp.it/stknaryb)
3. [input](https://clyp.it/men4ii4o) [output](https://clyp.it/yfseyz1h)


### 2. Quantization

#### MD-GAN + Kmeans
Clustering the latent space, using an MD-GAN with latent space dimension 128.
Here are some input clips and the output after encoding, quantization and reconstruction.
##### K=55 [input](https://clyp.it/gymirwql) [output](https://clyp.it/5u5pn2ii)
##### K=80 [input](https://clyp.it/ugh2pul0) [output](https://clyp.it/gqbgf4hy)


#### PCA + Kmeans
This experiment was performed by taking spectrograms segments of shape (16,129)
[16 timesteps of 129 frequency bins] and flattening them out into a vector
of length 129*16=2064. Then fitting PCA with 128 components with one part of the
dataset, then transforming another part of the dataset and training Kmeans on that data.
We then reconstruct the audio. Here are input and output samples for a few
number of clusters.

##### K=30 [input](https://clyp.it/flaetvxb) [output](https://clyp.it/nbpjcxqn)
##### K=55 [input](https://clyp.it/flaetvxb) [output](https://clyp.it/ms120mui)
##### K=180 [input](https://clyp.it/flaetvxb) [output](https://clyp.it/bnqp51t2)

#### Kmeans
Here we just do Kmeans on the spectrogram vector (1x2064 like above) directly.
##### K=30 [input](https://clyp.it/120yp3q5) [output](https://clyp.it/s4mjcl0o)
##### K=55 [input](https://clyp.it/120yp3q5) [output](https://clyp.it/zz05wz2j)
##### K=180 [input](https://clyp.it/120yp3q5) [output](https://clyp.it/hqa2n3gj)


### 3. Sequence generation
2 layer RNN with 512 LSTM units each. For details, see code in learn_sequences.py
This is generated using MD-GAN model with latent space dimension 128, and 
vocabulary size of 50.

[generated 1](https://clyp.it/ko4hzgfe)
