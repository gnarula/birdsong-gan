from __future__ import print_function
import os
import argparse
import random
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils
import numpy as np
from torch.autograd import Variable
from datetime import datetime
import itertools
import shutil
from scipy.stats import t as t_dist
from networks import _netG,_netE,_netD,weights_init,GANLoss
import pickle
from utils import *

parser = argparse.ArgumentParser()
parser.add_argument('--dataroot',default='', required=False, help='path to dataset')
parser.add_argument('--distance_fun',default='L2', required=False, help='choose L2 or L1 loss')
parser.add_argument('--workers', type=int, help='number of data loading workers', default=2)
parser.add_argument('--batchSize', type=int, default=16, help='input batch size')
parser.add_argument('--imageH',type=int, default=129, help='the height of the input image to network')
parser.add_argument('--imageW',type=int, default=16, help='the width of the input image to network')
parser.add_argument('--nfft', type=int, default=256, help='The nfft parameter of STFT used on the input data')
parser.add_argument('--nz', type=int, default=32, help='size of the latent z vector')
parser.add_argument('--ngf', type=int, default=64)
parser.add_argument('--nc', type=int, default=1)
parser.add_argument('--ndf', type=int, default=64)
parser.add_argument('--niter', type=int, default=25, help='number of epochs to train for')
parser.add_argument('--lr', type=float, default=0.0002, help='learning rate, default=0.0002')
parser.add_argument('--lambdaa', type=float, default=5.0, help='weight between terms of g loss')
parser.add_argument('--d_noise', type=float, default=0.0, help='add noise to discriminator labels (flip labels with this probability)')
parser.add_argument('--beta1', type=float, default=0.5, help='beta1 for adam. default=0.5')
parser.add_argument('--cuda' , action='store_true', help='enables cuda')
parser.add_argument('--ngpu'  , type=int, default=1, help='number of GPUs to use')
parser.add_argument('--netG', default='', help="path to netG (to continue training)")
parser.add_argument('--netD1', default='', help="path to netD1 (to continue training)")
parser.add_argument('--netD2', default='', help="path to netD2 (to continue training)")
parser.add_argument('--netD3', default='', help="path to netD2 (to continue training)")
parser.add_argument('--netE', default='', help="path to netE (to continue training)")
parser.add_argument('--log_every', type=int, default=200, help="how often to log")
parser.add_argument('--sample_rate', type=float, default=16000, help="sample rate of input data")
parser.add_argument('--outf', help='folder to output images and model checkpoints',default='/SP2/mdgan_output/')

def make_output_folder(path):
    """
    Creates output folder, also removes almost empty directories in the output path
    """
    if not os.path.exists(path):
        os.makedirs(path)
    dirs = os.listdir(path)
    for d in dirs:
        if len(os.listdir(os.path.join(path, d)))<=3:
            try:
                os.rmdir(os.path.join(path, d))
            except:
                shutil.rmtree(os.path.join(path,d))
    path += str(datetime.now()).replace(':', '-')
    if not os.path.exists(path):
        os.makedirs(path)
        os.makedirs(os.path.join(path,'losses'))
        os.makedirs(os.path.join(path, 'hist'))
    return path


def main():
    opt = parser.parse_args()
    opt.outf = make_output_folder(opt.outf)

    tmppath = os.path.join(opt.outf,'opt.pkl')
    with open(tmppath,'wb') as f:
        pickle.dump(opt,f)
    print(opt)

    opt.manualSeed = random.randint(1, 10000) # fix seed
    print("Random Seed: ", opt.manualSeed)
    random.seed(opt.manualSeed)
    torch.manual_seed(opt.manualSeed)

    cudnn.benchmark = True
    if torch.cuda.is_available() and not opt.cuda:
        print("WARNING: You have a CUDA device, so you should probably run with --cuda")

    def transform(im):
        """
        This function should be used to transform data into the desired format for the network.
        inverse transoform should be an inverse of this function
        """
        im = from_polar(im)
        im, phase = lc.magphase(im)
        im = np.log1p(im)
        return im

    def inverse_transform(im):
        """
        Inverse (or at least almost) of transofrm()
        """
        random_phase = im.copy()
        np.random.shuffle(random_phase)
        p = phase_restore((np.exp(im) - 1), random_phase, opt.nfft, N=50)
        return (np.exp(im) - 1) * p

    def im_loader(path):
        """
        This is the function between data on disk and the network.
        """
        im = np.load(path)
        im = random_crop(im, width=opt.imageW)
        im = transform(im)
        return im


    dataset = dset.ImageFolder(root=opt.dataroot,loader=im_loader)
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=opt.batchSize,
                                             shuffle=True, num_workers=int(opt.workers))
    ngpu = int(opt.ngpu)
    nz = int(opt.nz)
    ngf = int(opt.ngf)
    ndf = int(opt.ndf)
    nc = opt.nc
    # custom weights initialization called on netG and netD
    netG = _netG(ngpu,nz,ngf,nc)
    netG.apply(weights_init)
    if opt.netG != '':
        netG.load_state_dict(torch.load(opt.netG))
    print(netG)

    netD1 = _netD(ngpu,ndf,nc)
    netD1.apply(weights_init)
    if opt.netD1 != '':
        netD1.load_state_dict(torch.load(opt.netD1))
    print(netD1)

    netD2 = _netD(ngpu,ndf,nc)
    netD2.apply(weights_init)
    if opt.netD2 != '':
        netD2.load_state_dict(torch.load(opt.netD2))
    print(netD2)

    netD3 = _netD(ngpu,ndf,nc)
    netD3.apply(weights_init)
    if opt.netD3 != '':
        netD3.load_state_dict(torch.load(opt.netD3))
    print(netD3)


    if opt.cuda:
        criterion_gan = GANLoss(tensor=torch.cuda.FloatTensor,use_lsgan=False)
    else:
        criterion_gan = GANLoss()

    if opt.distance_fun=='L1':
        print('Using L1 loss')
        criterion_dist = nn.L1Loss()
    else:
        print('Using L2 loss')
        criterion_dist = nn.MSELoss()

    downsample_pth = torch.nn.AvgPool2d(3, stride=4)

    netE = _netE(ngpu,nz,ngf,nc)
    print(netE)
    netE.apply(weights_init)
    if opt.netE != '':
        netE.load_state_dict(torch.load(opt.netE))

    input = torch.FloatTensor(opt.batchSize, nc, opt.imageH, opt.imageW)
    if opt.cuda:
        netD1.cuda()
        netD2.cuda()
        netD3.cuda()
        netG.cuda()
        criterion_dist.cuda()
        input = input.cuda()
        netE.cuda()

    input = Variable(input)
    def noise_t():
        random_sample = t_dist.rvs(10,size=(batch_size,nz,1,1))
        out = Variable(torch.from_numpy(random_sample.astype(np.float32)))
        if opt.cuda:
            return out.cuda()
        else:
            return out
        #return Variable(torch.randn((batch_size,nz,1,1)).cuda())

    # setup optimizer
    optimizerD1 = optim.Adam(netD1.parameters(), lr = opt.lr, betas = (opt.beta1, 0.999))
    optimizerD2 = optim.Adam(netD2.parameters(), lr = opt.lr, betas = (opt.beta1, 0.999))
    optimizerD3 = optim.Adam(netD3.parameters(), lr = opt.lr, betas = (opt.beta1, 0.999))
    optimizerG = optim.Adam(itertools.chain(netG.parameters(), netE.parameters()), lr = opt.lr, betas = (opt.beta1, 0.999))

    #losses
    minibatchLossD1 = []
    minibatchLossG1_rec = []
    minibatchLossG1_gan = []
    minibatchLossD2 = []
    minibatchLossG2 = []
    minibatchLossD3 = []
    minibatchLossG_d3 = []

    #histograms
    hist_d1r = []
    hist_d2r = []
    hist_d3r = []
    hist_d1f = []
    hist_d2f = []
    hist_d3f = []
    hist_e = []
    hist_noise = []
    hist_G_means = []
    hist_G_stds = []
    hist_input_means = []
    hist_input_stds = []
    hist_rec_means = []
    hist_rec_stds = []

    d_prob = 1.0-opt.d_noise
    def true_wp(prob):
        if np.random.random() < prob:
            return True
        else:
            return False

    logpt = opt.log_every
    lambdaa = opt.lambdaa
    for epoch in range(opt.niter):
        for i, data in enumerate(dataloader):
            real_cpu, _ = data
            if opt.cuda:
                real_cpu = real_cpu.cuda()
            batch_size = real_cpu.size(0)
            rr = torch.Tensor(batch_size,nc,opt.imageH,opt.imageW)
            input.data.resize_(rr.size()).copy_(real_cpu)

            encoding = netE(input)
            netG.mode(reconstruction=True)
            reconstruction = netG(encoding)
            netD1.zero_grad()
            pred_rec_d1 = netD1(reconstruction.detach())
            pred_real_d1 = netD1(input)

            err_real_d1 = criterion_gan(pred_real_d1, true_wp(d_prob))
            err_fake_d1 = criterion_gan(pred_rec_d1, true_wp(1-d_prob))
            err_d1 = err_real_d1 + err_fake_d1
            err_d1.backward()
            optimizerD1.step()

            netG.zero_grad()
            netE.zero_grad()
            netD1.zero_grad()
            pred_rec_d1 = netD1(reconstruction)
            errG_discrim = criterion_gan(pred_rec_d1, True)
            errG_recon = (criterion_dist(reconstruction, input) + criterion_dist(downsample_pth(reconstruction),downsample_pth(input))) * lambdaa
            err_g_d1 = errG_discrim + errG_recon
            err_g_d1.backward()
            optimizerG.step()

            # ------------- Diffusion step ---------------
            netE.zero_grad()
            netG.zero_grad()
            encoding = netE(input)
            netG.mode(reconstruction=True)
            reconstruction = netG(encoding)
            netD2.zero_grad()
            pred_rec_d2 = netD2(reconstruction.detach())
            err_real_d2 = criterion_gan(pred_rec_d2, true_wp(d_prob))
            noise = noise_t()
            netG.mode(reconstruction=False)
            netG.zero_grad()
            fake = netG(noise)
            pred_fake_d2 = netD2(fake.detach())
            err_fake_d2 = criterion_gan(pred_fake_d2, true_wp(1-d_prob))
            err_d2 = err_real_d2 + err_fake_d2
            err_d2.backward()
            optimizerD2.step()

            netD2.zero_grad()
            pred_fake_d2 = netD2(fake)
            err_g_d2 = criterion_gan(pred_fake_d2, True)
            err_g_d2.backward()
            optimizerG.step()

            netD3.zero_grad()
            netG.mode(reconstruction=False)
            fake = netG(noise)
            pred_fake_d3 = netD3(fake.detach())
            pred_real_d3 = netD3(input)
            err_fake_d3 = criterion_gan(pred_fake_d3,False)
            err_real_d3 = criterion_gan(pred_real_d3,True)
            err_d3 = err_fake_d3+err_real_d3
            err_d3.backward()
            optimizerD3.step()

            err_g_d3 = criterion_gan(pred_fake_d3,True)

            # SAVE LOSSES
            minibatchLossD1.append(err_d1.data[0])
            minibatchLossD2.append(err_d2.data[0])
            minibatchLossD3.append(err_d3.data[0])
            minibatchLossG1_gan.append(errG_discrim.data[0])
            minibatchLossG1_rec.append(errG_recon.data[0])
            minibatchLossG_d3.append(err_g_d3.data[0])
            minibatchLossG2.append(err_g_d2.data[0])

            # SAVE HISTOGRAMS
            if (i % 50 == 0) & (i>0):
                hist_d1f.append(pred_rec_d1.data.cpu().numpy())
                hist_d1r.append(pred_real_d1.data.cpu().numpy())
                hist_d2f.append(pred_fake_d2.data.cpu().numpy())
                hist_d2r.append(pred_rec_d2.data.cpu().numpy())
                hist_d3f.append(pred_fake_d3.data.cpu().numpy())
                hist_d3r.append(pred_real_d3.data.cpu().numpy())
                hist_e.append(encoding.data.cpu().numpy())
                hist_noise.append(noise.data.cpu().numpy())
                hist_G_means.append(fake.data.cpu().numpy().mean())
                hist_G_stds.append(fake.data.cpu().numpy().std())
                hist_input_means.append(input.data.cpu().numpy().mean())
                hist_input_stds.append(input.data.cpu().numpy().std())
                hist_rec_means.append(reconstruction.data.cpu().numpy().mean())
                hist_rec_stds.append(reconstruction.data.cpu().numpy().std())

            ### SHOW LOSS AFTER SOME BATCHES ####
            if (i % logpt == 0) & (i > 0):
                print('[%d/%d][%d/%d] D1: %.2f (%.2f) D2: %.2f (%.2f) G1_gan: %.2f (%.2f) G1_rec: %.2f (%.2f) G2: %.2f (%.2f) D3: %.2f (%.2f)  G_D3: %.2f (%.2f)'
                      % (epoch, opt.niter, i, len(dataloader),
                        np.mean(minibatchLossD1[-logpt:]), np.std(minibatchLossD1[-logpt:]),
                         np.mean(minibatchLossD2[-logpt:]), np.std(minibatchLossD2[-logpt:]),
                         np.mean(minibatchLossG1_gan[-logpt:]), np.std(minibatchLossG1_gan[-logpt:]),
                         np.mean(minibatchLossG1_rec[-logpt:]), np.std(minibatchLossG1_rec[-logpt:]),
                         np.mean(minibatchLossG2[-logpt:]), np.std(minibatchLossG2[-logpt:]),
                         np.mean(minibatchLossD3[-logpt:]), np.std(minibatchLossD3[-logpt:]),
                         np.mean(minibatchLossG_d3[-logpt:]), np.std(minibatchLossG_d3[-logpt:])
                         )
                      )

                fixed_noise = noise_t()
                netG.mode(reconstruction=False)
                fake = netG(fixed_noise)
                out_shape = [opt.imageH,opt.imageW]
                fake_spectrograms = [get_spectrogram(inverse_transform(fake.data[k,:,:,:].cpu().numpy().reshape(out_shape))) for k in range(8)]
                fake_spectrograms = np.concatenate(fake_spectrograms,axis=1)
                save_spectrogram('%s/fake_samples_epoch_%03d_batchnumb_%d.png' % (opt.outf, epoch, i),fake_spectrograms)
                input_spectrograms = [get_spectrogram(inverse_transform(input.data[k,:,:,:].cpu().numpy().reshape(out_shape))) for k in range(8)]
                rec_spectrograms = [get_spectrogram(inverse_transform(reconstruction.data[k,:,:,:].cpu().numpy().reshape(out_shape))) for k in range(8)]
                rec_image = np.concatenate([input_spectrograms[k] if k%2==0 else rec_spectrograms[k-1] for k in range(8)],axis=1)
                save_spectrogram('%s/rec_samples_epoch_%03d_batchnumb_%d.png' % (opt.outf, epoch, i),rec_image)

                sample_file = get_random_sample(opt.dataroot)
                sample = np.load(sample_file)
#                save_audio_sample(from_image(sample),
#                                  '%s/input_audio_epoch_%03d_batchnumb_%d.wav' % (opt.outf, epoch, i), opt.sample_rate)
                save_audio_sample(lc.istft(inverse_transform(transform(sample))),
                                  '%s/input_audio_epoch_%03d_batchnumb_%d.wav' % (opt.outf, epoch, i), opt.sample_rate)

                sample_segments = segment_image(sample,width=opt.imageW)
                sample_segments = [transform(k) for k in sample_segments]
                sample_batches,num_segments = to_batches(sample_segments,opt.batchSize)
                netG.mode(reconstruction=True)
                reconstructed_samples = []
                cnt = 0
                for j in range(len(sample_batches)):
                    input.data.copy_(torch.from_numpy(sample_batches[j]))
                    encoding = netE(input)
                    reconstruction = netG(encoding)
                    for k in range(reconstruction.data.cpu().numpy().shape[0]):
                        if cnt<num_segments:
                            reconstructed_samples.append(inverse_transform(reconstruction.data[k,:,:,:].cpu().numpy().reshape(out_shape)))
                            cnt+=1

                reconstructed_samples = np.concatenate(reconstructed_samples,axis=1)
                reconstructed_audio = lc.istft(reconstructed_samples)
                save_audio_sample(reconstructed_audio,'%s/rec_audio_epoch_%03d_batchnumb_%d.wav' % (opt.outf, epoch, i),opt.sample_rate)


                losspath = os.path.join(opt.outf, 'losses/')
                histpath = os.path.join(opt.outf,'hist/')
                np.save(losspath+'D1',np.array(minibatchLossD1))
                np.save(losspath+'D2',np.array(minibatchLossD2))
                np.save(losspath+'D3',np.array(minibatchLossD3))
                np.save(losspath+'G1rec',np.array(minibatchLossG1_rec))
                np.save(losspath+'G1gan',np.array(minibatchLossG1_gan))
                np.save(losspath+'G2',np.array(minibatchLossG2))
                np.save(losspath+'G3',np.array(minibatchLossG_d3))

                np.save(histpath+'D1f',np.array(hist_d1f))
                np.save(histpath+'D1r',np.array(hist_d1r))
                np.save(histpath+'D2f',np.array(hist_d2f))
                np.save(histpath+'D2r',np.array(hist_d2r))
                np.save(histpath+'D3f',np.array(hist_d3f))
                np.save(histpath+'D3r',np.array(hist_d3r))
                np.save(histpath+'E',np.array(hist_e))
                np.save(histpath+'noise',np.array(hist_noise))
                np.save(histpath+'Gmeans',np.array(hist_G_means))
                np.save(histpath+'Gstds',np.array(hist_G_stds))
                np.save(histpath+'inputmeans',np.array(hist_input_means))
                np.save(histpath+'inputstds',np.array(hist_input_stds))
                np.save(histpath+'recmeans',np.array(hist_rec_means))
                np.save(histpath+'recstds',np.array(hist_rec_stds))

        # do checkpointing
        torch.save(netG.state_dict(), '%s/netG_epoch_%d.pth' % (opt.outf, epoch))
        torch.save(netD1.state_dict(), '%s/netD1_epoch_%d.pth' % (opt.outf, epoch))
        torch.save(netD2.state_dict(), '%s/netD2_epoch_%d.pth' % (opt.outf, epoch))
        torch.save(netD3.state_dict(), '%s/netD3_epoch_%d.pth' % (opt.outf, epoch))
        torch.save(netE.state_dict(), '%s/netE_epoch_%d.pth' % (opt.outf, epoch))


if __name__ == '__main__':
    main()