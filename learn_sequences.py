from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.utils.data_utils import get_file
from keras.preprocessing.sequence import pad_sequences
from os.path import join
import pickle
import numpy as np
import random
import sys
import os
from utils import *

base_path = "/media/songbird/Data/mdgan_output/2017-11-13 13-56-24.027163/"
base_path = "../mdgan_output/2017-11-13 13-56-24.027163/"
seq_path = "/home/songbird/jit_syllable_songbird/svenni/model_13-56-24_seq_as_ints_k50"
seq_path = "../learn_seq/model_13-56-24_seq_as_ints_k50"
dataset_files = os.listdir(seq_path)

output_path = join(base_path,'learn_sequences')
vocab_size = 50+3
start_token = vocab_size-3
end_token = vocab_size-2
pad_token = vocab_size-1
n_samples = 100000

def parse_line(line):
    return [int(i) for i in line.replace('\n','').split(',')[:-1]]

lines = []
for f in dataset_files:
    tmp = open(join(seq_path,f),'r').readlines()
    lines = lines+[parse_line(i) for i in tmp]

np.random.shuffle(lines)
maxlen = 40
step = 1
print("maxlen:",maxlen,"step:", step)

def make_sequences(line):
    seq_out = []
    next_symbols = []
    for i in range(len(line)):
        seq = [start_token]
        seq += line[:i]
        next_symbols.append(line[i])
        seq_out.append(seq)
    seq_out.append(line)
    next_symbols.append(end_token)
    return seq_out,next_symbols

def decode_sequences(data_path):
    files = os.listdir(data_path)
    files = [i for i in files if '.npy' in i]
    samples = [np.load(join(data_path,f)) for f in files]
    print("saving %d sequences as .wav files"%(len(files)))
    os.makedirs(join(data_path,'wav_files'),exist_ok=True)
    for i,sample in enumerate(samples):
        rec = reconstruct_sample(base_path,sample,epoch=2)
        sf.write(join(data_path,'wav_files',files[i].replace('.npy','.wav')),rec,samplerate=16000)


sentences = []
next_words = []
for line in lines:
    sents,nexts = make_sequences(line)
    sentences+=sents
    next_words+=nexts

print("number of sequences: ",len(sentences))
sentences=sentences[:n_samples]
next_words=next_words[:n_samples]

sentences = pad_sequences(sentences,truncating='post',value=pad_token,maxlen=maxlen)

print('Vectorization...')
X = np.zeros((len(sentences), maxlen, vocab_size), dtype=np.bool)
y = np.zeros((len(sentences), vocab_size), dtype=np.bool)
for i, sentence in enumerate(sentences):
    for t, word in enumerate(sentence):
        X[i, t, word] = 1
    y[i, next_words[i]] = 1


#build the model: 2 stacked LSTM
print('Build model...')
model = Sequential()
model.add(LSTM(512, return_sequences=True, input_shape=(maxlen, vocab_size)))
model.add(Dropout(0.2))
model.add(LSTM(512, return_sequences=False))
model.add(Dropout(0.2))
model.add(Dense(vocab_size))
#model.add(Dense(1000))
model.add(Activation('softmax'))

model.compile(loss='categorical_crossentropy', optimizer='rmsprop')

if os.path.isfile(join(output_path,'model.h5')):
    model.load(join(output_path,'model.h5'))

def sample(a, temperature=1.0):
    # helper function to sample an index from a probability array
    a = np.log(a) / temperature
    dist = np.exp(a) / np.sum(np.exp(a))
    choices = range(len(a))
    return np.random.choice(choices, p=dist)

# train the model, output generated text after each iteration
kmeans=pickle.load(open(os.path.join(base_path,'kmeans50.pkl'),'rb'))
os.makedirs(output_path,exist_ok=True)

def generate(model,diversity):
    generated = []
    sentence = list(pad_sequences([[start_token]], maxlen=maxlen, value=pad_token)[0])
    for i in range(1024):
        x = np.zeros((1, maxlen, vocab_size))
        for t, word in enumerate(sentence):
            x[0, t, word] = 1.
        preds = model.predict(x, verbose=0)[0]
        next_index = sample(preds, diversity)
        next_word = next_index
        if next_word == pad_token or next_word == start_token:
            continue
        if next_word == end_token:
            break
        generated.append(next_word)
        del sentence[0]
        sentence.append(next_word)
    return generated

for iteration in range(0, 20):
    print()
    print('-' * 50)
    print('Iteration', iteration)
    model.fit(X, y, batch_size=128, nb_epoch=1)
    model.save("model.h5")
    for diversity in np.arange(0.1,1.0,0.1):
        print()
        print('----- diversity:', diversity)
        generated = generate(model,diversity)
        z_seq = []
        for g in generated:
            z_seq.append(kmeans.cluster_centers_[g])
        z_seq = np.array(z_seq)
        np.save(join(output_path,'generated_iteration_%d_diversity_%.2f'%(iteration,diversity)),z_seq)
        print(generated)

for diversity in np.arange(0.2, 1.0, 0.1):
    z_seq_long = []
    for i in range(50):
        generated = generate(model,diversity)
        for g in generated:
            z_seq_long.append(kmeans.cluster_centers_[g])

    z_seq_long = np.array(z_seq_long)
    np.save(join(output_path, 'long_diversity_%.2f' % (diversity)), z_seq_long)

    #model.save_weights('weights')