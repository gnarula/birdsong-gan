import os
import pickle
import numpy as np
from scipy.stats import t as t_dist
from torch.autograd import Variable
import torch
import sys
import torchvision.utils as vutils
from utils import *
from torch import nn
import argparse
import torchvision.datasets as dset
from sklearn.cluster import KMeans



parser = argparse.ArgumentParser()
parser.add_argument('--model_path',default='/media/songbird/Data/mdgan_output/', required=False, help='path to models')
parser.add_argument('--data_path',default='', required=True, help='path to dataset')
parser.add_argument('--seq_path',default='', required=True, help='path to sequences to train kmeans')
parser.add_argument('--run',default='', required=True, help='name of run')
parser.add_argument('--epoch', type=int, required=True, help='what model to use')
parser.add_argument('--max_num_kmeans',type=int, required=True, help='max number of data points to train kmeans')
parser.add_argument('--max_num_val',type=int,default=-1, required=False, help='max number of batches to validate on')
parser.add_argument('--save_samples',type=int,default=20, required=False, help='number of sample songs to reconstruct')


"""
After running generate_sequences.py with a model this script can be used to train k-means classifiers on the data in 
the latent space and calculate the reconsturction error with that classifier. The script saves some quantized birddsongs with
each k-means classifier.
"""

def im_loader(path):
    im = np.load(path)
    im = random_crop(im,width=16)
    im = transform(im)
    return im

def transform(im):
    im = from_polar(im)
    im,phase = lc.magphase(im)
    im = np.log1p(im)
    return im

def inverse_transform(im):
    random_phase = im.copy()
    np.random.shuffle(random_phase)
    p = phase_restore((np.exp(im) - 1), random_phase, 256, N=200)
    return (np.exp(im)-1)*p

def load_z(path,max_num=None):
    z_vecs=[]
    for f in os.listdir(path):
        if max_num:
            if len(z_vecs)>=max_num:
                break
        s = np.load(os.path.join(path,f))
        _ = [z_vecs.append(k) for k in s if not np.sum(np.abs(k))==0]
    return np.array(z_vecs)

def quantize(clf,sample):
    return np.array([clf.cluster_centers_[i] for i in clf.predict(sample)])


def main():
    args = parser.parse_args()

    runs = os.listdir(args.model_path)
    run = args.run

    for i in range(len(runs)):
        if run in runs[i]:
            path = os.path.join(args.model_path,runs[i])
            break

    with open(os.path.join(path, 'opt.pkl'), 'rb') as f:
        opt = pickle.load(f)
    print("testing: ",path)

    out_path = join(path,'validation_kmeans')
    os.makedirs(out_path,exist_ok=True)

    ngpu=opt.ngpu
    nz=opt.nz
    ngf=opt.ngf
    nc=opt.nc
    ndf=opt.ndf
    if torch.cuda.is_available() and not opt.cuda:
        print("WARNING: You have a CUDA device, so you should probably run with --cuda")


    dataset_train = dset.ImageFolder(root=args.data_path,loader=im_loader)
    dataloader_train = torch.utils.data.DataLoader(dataset_train, batch_size=opt.batchSize,
                                             shuffle=True, num_workers=int(opt.workers))
    dataset_val = dset.ImageFolder(root=args.data_path+'_val',loader=im_loader)
    dataloader_val = torch.utils.data.DataLoader(dataset_val, batch_size=opt.batchSize,
                                             shuffle=True, num_workers=int(opt.workers))

    directory = args.data_path
    dirs = [i for i in os.listdir(directory) if not len(os.listdir(join(directory,i)))==0]
    def random_sample():
        rand_dir = dirs[np.random.randint(len(dirs))]
        sample_files = os.listdir(join(directory,rand_dir))
        return join(directory,rand_dir,sample_files[np.random.randint(len(sample_files))])

    net_G_file = os.path.join(path, 'netG_epoch_%d.pth' % (args.epoch))
    net_E_file = os.path.join(path, 'netE_epoch_%d.pth' % (args.epoch))

    try:
        from networks_1d import _netG, _netE, _netD, weights_init, GANLoss
        netG = _netG(ngpu, nz, ngf, nc)
        netG.apply(weights_init)
        netG.load_state_dict(torch.load(net_G_file))
        netE = _netE(ngpu, nz, ngf, nc)
        netE.apply(weights_init)
        netE.load_state_dict(torch.load(net_E_file))
    except:
        from networks_audio_nophase import _netG, _netE, _netD, weights_init, GANLoss
        netG = _netG(ngpu, nz, ngf, nc)
        netG.apply(weights_init)
        netG.load_state_dict(torch.load(net_G_file))
        netE = _netE(ngpu, nz, ngf, nc)
        netE.apply(weights_init)
        netE.load_state_dict(torch.load(net_E_file))

    netG.cuda()
    netE.cuda()

    if args.max_num_val == -1:
        max_num_val = min(len(dataloader_train), len(dataloader_val))
    else:
        max_num_val = args.max_num_val
    max_num_kmeans = args.max_num_kmeans

    seq_folders = [i for i in os.listdir(args.seq_path)]
    X = np.concatenate([load_z(join(args.seq_path, i), max_num=max_num_kmeans) for i in seq_folders])
    criterion_dist = nn.MSELoss()
    np.random.shuffle(X)
    k_vals = np.arange(5,300,25)
    errors = []
    stds = []
    errors_q = []
    stds_q = []
    input = torch.FloatTensor(opt.batchSize, nc, opt.imageH, opt.imageW)
    input = input.cuda()
    input = Variable(input)
    criterion_dist.cuda()


    for k in k_vals:
        print("Fitting kmeans clf with %d clusters, %d datapoints"%(k,len(X)))
        Z = X
        Z = Z[:1000000]
        clf = KMeans(n_clusters=k, n_jobs=4)
        clf.fit(Z)
        with open(join(out_path, 'kmeans_%d.pkl'%(k)), 'wb') as f:
            pickle.dump(clf, f)

        val_error = []
        val_q_error = []
        for i, data in enumerate(dataloader_val):
            if i>=max_num_val:
                break
            real_cpu, _ = data
            real_cpu.cuda()
            batch_size = real_cpu.size(0)
            rr = torch.Tensor(batch_size,nc,opt.imageH,opt.imageW)
            input.data.resize_(rr.size()).copy_(real_cpu)
            encoding = netE(input)
            netG.mode(reconstruction=True)
            reconstruction = netG(encoding)
            encoding_q = quantize(clf,encoding.data.cpu().numpy())
            encoding_q_var = Variable(torch.from_numpy(encoding_q.astype(np.float32)).cuda())
            encoding_q_var = encoding_q_var.cuda()
            reconstruction_q = netG(encoding_q_var)
            val_error.append(criterion_dist(reconstruction, input).data[0])
            val_q_error.append(criterion_dist(reconstruction_q, input).data[0])
        errors.append(np.mean(val_error))
        errors_q.append(np.mean(val_q_error))
        stds.append(np.std(val_error))
        stds_q.append(np.std(val_q_error))
        sample_out_path = join(out_path,"samples_%d_clusters"%(k))
        os.makedirs(sample_out_path,exist_ok=True)
        print("saving samples...")
        for i in range(args.save_samples):
            sample = np.load(random_sample())
            sf.write(join(sample_out_path, "sample_input_%d.wav" % (i)), from_image(sample), samplerate=16000)
            enc = encode_sample(path, sample, epoch=args.epoch)
            rec = reconstruct_sample(path, enc, epoch=args.epoch)
            enc_q = quantize(clf, enc)
            rec_q = reconstruct_sample(path, enc_q, epoch=args.epoch)
            sf.write(join(sample_out_path, "sample_rec_%d.wav" % (i)), rec, samplerate=16000)
            sf.write(join(sample_out_path,"sample_quantized_%d.wav" % (i)), rec_q, samplerate=16000)
        print("k: ",k)
        print("error: ",errors[-1],"  std: ",stds[-1])
        print("error quantized: ",errors_q[-1],"   std: ",stds_q[-1])
    with open(join(out_path,'validation.csv'),'w') as f:
        f.write("k,err_mean,err_std,q_err_mean,q_err_std\n")
        for i in range(len(errors)):
            f.write("%d,%.4f,%.4f,%.4f,%.4f\n"%(k_vals[i],errors[i],stds[i],errors_q[i],stds_q[i]))


if __name__=='__main__':
    main()
