import sys, os
import numpy as np
import torch
from torch.autograd import Variable
import soundfile as sf
import wave
from sklearn.model_selection import train_test_split
from scipy.signal import resample
import librosa.core as lc
from librosa.util import fix_length
from sklearn.utils import shuffle
from scipy.misc import imsave
from datetime import datetime as dt
from PIL import Image
import librosa
import sounddevice as sd
import pickle
from sklearn.model_selection import train_test_split
from os.path import join

if not sys.platform=='linux':
    """Because it fails when I use the linux machine remotely"""
    import librosa.display
    import matplotlib as mpl
    import matplotlib.pyplot as plt


def random_crop(im,width=8):
	ceil = im.shape[1]-width
	ind = np.random.randint(ceil)
	return im[:,ind:ind+width]


def segment_image(im,width=8):
    segments = [im[:,i*width:(i+1)*width] for i in range(im.shape[1]//width)]
    return segments

def to_batches(segments,batch_size):
    n_batches = int(np.ceil(len(segments)/batch_size))
    batches = [np.zeros(shape=(batch_size,)+tuple(segments[0].shape)) for i in range(n_batches)]
    for i in range(len(segments)):
        batch_idx = i//batch_size
        idx = i%batch_size
        batches[batch_idx][idx] = segments[i]
    return np.array(batches), len(segments)

def get_random_sample(directory):
    try:
        files = os.listdir(join(directory,'images'))
        randint = np.random.randint(len(files))
        return join(directory,'images',files[randint])
    except:
        dirs = [i for i in os.listdir(directory) if not len(os.listdir(join(directory,i)))==0]
        rand_dir = dirs[np.random.randint(len(dirs))]
        files = os.listdir(join(directory,rand_dir))
        return join(directory,rand_dir,files[np.random.randint(len(files))])

def downsample(x,down_factor):
    n = x.shape[0]
    y = np.floor(np.log2(n))
    nextpow2 = int(np.power(2, y + 1))
    x = np.concatenate((np.zeros((nextpow2-n), dtype=x.dtype), x))
    x = resample(x,len(x)//down_factor)
    return x[(nextpow2-n)//down_factor:]

def play_clip(data, fs=44100):
    sd.play(data, fs)

def play_file(file_path):
    data,fs = sf.read(file_path)
    print("playing from file: ",file_path)
    sd.play(data, fs)

def load_from_folder(base_path,folder_path):
    files = os.listdir(join(base_path,folder_path,'songs'))
    files = [i for i in files if '.wav' in i.lower()]
    data = [sf.read(join(base_path,folder_path,'songs',i)) for i in files]
    rates = [i[1] for i in data]
    data = [i[0] for i in data]
    if len(data)==0:
        return None,None
    if len(set(rates))>1:
        print("Sample rates are not the same")
        print("Sample rates are: ")
        print(rates)
    else:
        print("Sample rate is : ",rates[0])
    return data,rates[0]

def play(f):
    import pyaudio
    CHUNK = 1024
    print('playing file: ' + f.split('\\')[-1])
    wf = wave.open(f, 'rb')
    p = pyaudio.PyAudio()
    stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                    channels=wf.getnchannels(),
                    rate=wf.getframerate(),
                    output=True)
    data = wf.readframes(CHUNK)
    try:
        while data != '':
            stream.write(data)
            data = wf.readframes(CHUNK)
    except KeyboardInterrupt:
        pass
    stream.stop_stream()
    stream.close()
    p.terminate()


def normalize_image(image):
    return image / np.std(image)

def update_progress(progress):
    print("\rProgress: [{0:50s}] {1:.1f}%".format('#' * int(progress * 50),progress * 100),end='')


def phase_restore(mag, random_phases,n_fft, N=50):
    p = np.exp(1j * (random_phases))

    for i in range(N):
        _, p = librosa.magphase(librosa.stft(
            librosa.istft(mag * p), n_fft=n_fft))
    #    update_progress(float(i) / N)
    return p


def to_image(seq,nfft):
    """
    :param seq:  Raw audio
    :param nfft: parameter of STFT
    :return: STFT of the input seq, broken down into magnitude in one channel and phase in the other.
    """
    nfft_padlen = int(len(seq) + nfft / 2)
    stft = lc.stft(fix_length(seq, nfft_padlen), n_fft=nfft)
    return np.array([np.abs(stft), np.angle(stft)]).transpose(1, 2, 0)

def from_polar(image):
    """
    :param image: STFT in "image form" with magnitude in one channel and phase in the other.
    :return: The STFT in its original form.
    """
    return image[:, :, 0]*np.cos(image[:, :, 1]) + 1j*image[:,:,0]*np.sin(image[:,:,1])


def from_image(image):
    """
    :param image: STFT in "image form" with magnitude in one channel and phase in the other.
    :return: Raw audio
    """
    return lc.istft(from_polar(image))

def save_image(image,save_path,save_idx,amplitude_only=False):
    if amplitude_only:
        np.save(join(save_path, str(save_idx) + '.npy'), image[:,:,0])
    else:
        np.save(join(save_path, str(save_idx) + '.npy'), image)

def get_spectrogram(data,log_scale=False,show=False,polar_form_input=False,save_file=None,figsize=(12,8)):
    if polar_form_input:
        image = from_polar(data)
    elif len(data.shape)==2:
        image=data
    else:
        image = lc.stft(data)
    D = librosa.amplitude_to_db(image, ref=np.max)
    if show or save_file:
        plt.figure(figsize=figsize)
        if log_scale:
            librosa.display.specshow(D, y_axis='log')
        else:
            librosa.display.specshow(D, y_axis='linear')
        plt.colorbar(format='%+2.0f dB')
        plt.title('Frequency power spectrogram')
        if save_file:
            plt.savefig(save_file)
        if show:
            plt.show()
    return D

def specshow(data):
    D = librosa.amplitude_to_db(data, ref=np.max)
    plt.figure(figsize=(16,6))
    librosa.display.specshow(D, y_axis='linear',sr=16000)
    plt.colorbar(format='%+2.0f dB')
    plt.show()


def save_spectrogram(filename,D):
    if D.min() < 0:
        D = D-D.min()
    D = D/D.max()
    I = Image.fromarray(np.uint8(D*255))
    I.save(filename)


def save_audio_sample(sample,path,samplerate):
    sf.write(path,sample,samplerate=int(samplerate))



def normalize_spectrogram(image,threshold):
    if threshold>1.0:
        image = image/threshold
    image = np.minimum(image,np.ones(shape=image.shape))
    image = np.maximum(image,-np.ones(shape=image.shape))
    return image


def generate_dataset(base_path,out_path,nfft,max_num=None,downsample_factor=None):
    """
    :param base_path: Path to the directory containing directories that contain raw audio files
    :param out_path: Where to save the genearted dataset
    :param nfft: parameter of STFT
    :param max_num: Max number of samples to genearte (if None, then genearet with entire set)
    :param downsample_factor: The original data is sampled at 32kHz, if  for exampel, downsample_factor is 2, then the
                              STFT is computed on the audio after resampling to 16kHz
    """
    save_idx=0
    folders = os.listdir(base_path)
    out_path = join(out_path,'images')
    os.makedirs(out_path,exist_ok=True)
    for folder in folders:
        songs, fs = load_from_folder(base_path, folder)
        if songs:
            if downsample_factor:
                songs = [downsample(i, downsample_factor) for i in songs]
            ims = [to_image(i,nfft) for i in songs]
            for im in ims:
                save_image(im,out_path,save_idx)
                save_idx+=1
                if max_num:
                    if save_idx>=max_num:
                        return


def inverse_transform(im,N=50):
    random_phase = im.copy()
    np.random.shuffle(random_phase)
    p = phase_restore((np.exp(im) - 1), random_phase, 256, N=N)
    return (np.exp(im) - 1) * p

def transform(im):
    im = from_polar(im)
    im,phase = lc.magphase(im)
    im = np.log1p(im)
    return im

def generate_dataset_by_day(base_path,out_path,nfft,downsample_factor=None,train_val_split=False,name=''):
    """
    :param base_path: Path to the directory containing directories that contain raw audio files
    :param out_path: Where to save the genearted dataset
    :param nfft: parameter of STFT
    :param max_num: Max number of samples to genearte (if None, then genearet with entire set)
    :param downsample_factor: The original data is sampled at 32kHz, if  for exampel, downsample_factor is 2, then the
                              STFT is computed on the audio after resampling to 16kHz
    :param train_val_split: If True then save 5% of the data as validation set and 5% as test set
    """
    save_idx=0
    folders = os.listdir(base_path)
    days = []
    for f in folders:
        try:
            day = dt.strptime('-'.join(f.split('-')[:3]), '%Y-%m-%d')
            days.append(day)
        except:
            continue
    days.sort()
    amplitude_only = False
    for folder in folders:
        try:
            day = dt.strptime('-'.join(folder.split('-')[:3]), '%Y-%m-%d')
            num = (day-days[0]).days
            out_path_full = join(out_path,"day%d_%s"%(num,name),"images")
        except:
            out_path_full = join(out_path,folder+'_'+name,"images")

        os.makedirs(out_path_full, exist_ok=True)
        songs, fs = load_from_folder(base_path, folder)
        if not songs:
            continue
        if downsample_factor:
            songs = [downsample(i, downsample_factor) for i in songs]
        ims = [to_image(i,nfft) for i in songs]
        if train_val_split:
            out_path_test = out_path_full.replace(out_path,out_path[:-1]+'_test/')
            out_path_val = out_path_full.replace(out_path,out_path[:-1]+'_val/')
            os.makedirs(out_path_test, exist_ok=True)
            os.makedirs(out_path_val, exist_ok=True)
            im_train,im_test = train_test_split(ims,test_size=0.1)
            im_val,im_test = train_test_split(im_test,test_size=0.5)
            for im in im_train:
                save_image(im, out_path_full, save_idx, amplitude_only=amplitude_only)
                save_idx+=1
            for im in im_val:
                save_image(im, out_path_val, save_idx, amplitude_only=amplitude_only)
                save_idx+=1
            for im in im_test:
                save_image(im, out_path_test, save_idx, amplitude_only=amplitude_only)
                save_idx+=1
        else:
            for im in ims:
                save_image(im,out_path_full,save_idx,amplitude_only=amplitude_only)
                save_idx+=1


def encode_sample(path,sample,epoch=None):
    """
    :param path: Model output path
    :param sample: sample to encode
    :param epoch: which epoch of the model to use
    :return: Encoding of the sample
    """
    with open(join(path, 'opt.pkl'), 'rb') as f:
        opt = pickle.load(f)
    ngpu = opt.ngpu
    nz = opt.nz
    ngf = opt.ngf
    nc = opt.nc
    input = torch.FloatTensor(opt.batchSize, nc, opt.imageH, opt.imageW)
    input = Variable(input)

    if epoch is not None:
        net_E_file = join(path,'netE_epoch_%d.pth'%(epoch))
    else:
        E_files = [i for i in os.listdir(path) if 'netE' in i]
        net_E_file = join(path,'netE_epoch_%d.pth'%(len(E_files)-1))
    try:
        from networks_1d import _netG, _netE, _netD, weights_init, GANLoss
        netE = _netE(ngpu, nz, ngf, nc)
        netE.apply(weights_init)
        netE.load_state_dict(torch.load(net_E_file))
        opt.imageH=opt.nc
    except:
        from networks_audio_nophase import _netG, _netE, _netD, weights_init, GANLoss
        netE = _netE(ngpu, nz, ngf, nc)
        netE.apply(weights_init)
        netE.load_state_dict(torch.load(net_E_file))

    netE.cuda()
    encoded = []
    sample_segments = segment_image(sample, width=opt.imageW)
    sample_segments = [transform(k) for k in sample_segments]
    sample_batches, num_segments = to_batches(sample_segments, opt.batchSize)
    cnt = 0
    sequence=[]
    for j in range(len(sample_batches)):
        input.data.copy_(torch.from_numpy(sample_batches[j]))
        encoding = netE(input)
        for k in range(opt.batchSize):
            if cnt >= num_segments:
                sequence.append(np.zeros(shape=sequence[-1].shape))
            else:
                sequence.append(encoding.data[k].cpu().numpy())
                cnt += 1
    return np.array([i for i in sequence if not np.sum(np.abs(i))==0])

def reconstruct_sample(path,sample,epoch=None):
    """
    :param path: Model to use to reconstruct sample
    :param sample: A sample in the latent space
    :param epoch: Which epoch of the model to use
    :return: output of the generator, given the input sample
    """
    with open(join(path, 'opt.pkl'), 'rb') as f:
        opt = pickle.load(f)
    ngpu = opt.ngpu
    nz = opt.nz
    ngf = opt.ngf
    nc = opt.nc

    if epoch is not None:
        net_G_file = join(path,'netG_epoch_%d.pth'%(epoch))
    else:
        G_files = [i for i in os.listdir(path) if 'netG' in i]
        net_G_file = join(path,'netG_epoch_%d.pth'%(len(G_files)-1))
        if not os.path.isfile(net_G_file):
            net_G_file = G_files[-1]

    try:
        from networks_1d import _netG, _netE, _netD, weights_init, GANLoss
        netG = _netG(ngpu, nz, ngf, nc)
        netG.apply(weights_init)
        netG.load_state_dict(torch.load(net_G_file))
        opt.imageH=opt.nc
    except:
        from networks_audio_nophase import _netG, _netE, _netD, weights_init, GANLoss
        netG = _netG(ngpu, nz, ngf, nc)
        netG.apply(weights_init)
        netG.load_state_dict(torch.load(net_G_file))

    netG.cuda()
    netG.mode(reconstruction=True)
    reconstructed_samples = []
    cnt=0
    if len(sample)%opt.batchSize==0:
        r=len(sample)//opt.batchSize
    else:
        r=len(sample)//opt.batchSize + 1
    for j in range(r):
        encoding = Variable(torch.from_numpy(sample[j*opt.batchSize:(j+1)*opt.batchSize].astype(np.float32))).cuda()
        reconstruction = netG(encoding)
        for k in range(reconstruction.data.cpu().numpy().shape[0]):
            if cnt>=len(sample):
                break
            reconstructed_samples.append(
                inverse_transform(reconstruction.data[k, :, :, :].cpu().numpy().reshape([opt.imageH,opt.imageW]),N=500))
            cnt+=1
    reconstructed_samples = np.concatenate(reconstructed_samples, axis=1)
    reconstructed_audio = lc.istft(reconstructed_samples)
    return reconstructed_audio
