import os
import pickle
import numpy as np
from scipy.stats import t as t_dist
from torch.autograd import Variable
import torch
import sys
import torchvision.utils as vutils
from utils import *
import argparse
import torchvision.datasets as dset

from torch import nn

parser = argparse.ArgumentParser()
parser.add_argument('--model_path',default='/media/songbird/Data/mdgan_output/', required=True, help='path to models')
parser.add_argument('--data_path',default='', required=True, help='path to dataset')
parser.add_argument('--out_path',default='', required=False, help='if not used then output is saved to same folder as model')
parser.add_argument('--run',default='', required=True, help='name of run')
parser.add_argument('--split_days',action='store_true', required=False, help='split dataset by days')
parser.add_argument('--max_num',default=-1, required=False, help='max number of sequences')
parser.add_argument('--epoch',type=int,default=-1, required=False, help='model after epoch to load, if -1: load latest')

"""
This script uses the encoder of an mdgan model to transform the dataset into sequences of samples in the latent space
"""

def transform(im):
    im = from_polar(im)
    im,phase = lc.magphase(im)
    im = np.log1p(im)
    return im


def im_loader(path):
    im = np.load(path)
    im = transform(im)
    return im


def process_folder(in_path,out_path,opt,args,netE):

    ngpu=opt.ngpu
    nz=opt.nz
    ngf=opt.ngf
    nc=opt.nc
    ndf=opt.ndf
    try:
        dataset = dset.ImageFolder(root=in_path,loader=im_loader)
    except:
        print("No data in folder: ",in_path)
        return
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=1,
                                             shuffle=True, num_workers=int(opt.workers))
    input = torch.FloatTensor(opt.batchSize, nc, opt.imageH, opt.imageW)
    input = Variable(input)
    N = len(dataloader)

    buffer = []
    os.makedirs(out_path,exist_ok=True)
    batch_size = opt.batchSize
    for i, data in enumerate(dataloader):
        real_cpu, day = data
        day = dataset.classes[day.cpu().numpy().astype(np.int32)[0]]
        if not os.path.isdir(join(out_path,day)):
            os.makedirs(join(out_path,day))
        sample_segments = segment_image(real_cpu.numpy().reshape(real_cpu.numpy().shape[1:]), width=opt.imageW)
        sample_batches, num_segments = to_batches(sample_segments, batch_size)
        cnt = 0
        for j in range(len(sample_batches)):
            input.data.copy_(torch.from_numpy(sample_batches[j]))
            encoding = netE(input)
            for k in range(batch_size):
                if cnt>=num_segments:
                    buffer.append(np.zeros(shape=buffer[-1].shape))
                else:
                    buffer.append(encoding.data[k].cpu().numpy())
                    cnt+=1

        sequence = np.array(buffer)
        buffer = []
        np.save(os.path.join(out_path,day,'seq_%d.npy'%(i)),sequence)
        if i >= args.max_num and args.max_num!=-1:
            break
        if (i%100)==0:
            print("Generated %d / %d sequences"%(i,N))

def main():
    args = parser.parse_args()

    base_path = args.model_path

    runs = os.listdir(base_path)
    run = args.run
    for i in range(len(runs)):
        if run in runs[i]:
            path = os.path.join(base_path,runs[i])
            break

    with open(os.path.join(path, 'opt.pkl'), 'rb') as f:
        opt = pickle.load(f)

    ngpu=opt.ngpu
    nz=opt.nz
    ngf=opt.ngf
    nc=opt.nc
    ndf=opt.ndf
    print("Generating sequences with model: ",path)

    pth_files = [i for i in os.listdir(path) if 'netE' in i]
    if args.epoch==-1:
        if os.path.isfile(os.path.join(path,'netE_epoch_%d.pth'%(len(pth_files)-1))):
            net_E_file = os.path.join(path,'netE_epoch_%d.pth'%(len(pth_files)-1))
        else:
            net_E_file = os.path.join(path,pth_files[-1])
    else:
        net_E_file = os.path.join(path, 'netE_epoch_%d.pth' % (args.epoch))

    print("Reading encoder model: ",net_E_file)
    try:
        from networks_1d import _netE, weights_init
        netE = _netE(ngpu, nz, ngf, nc)
        netE.apply(weights_init)
        netE.load_state_dict(torch.load(net_E_file))
    except:
        from networks_audio_nophase import _netE, weights_init
        netE = _netE(ngpu, nz, ngf, nc)
        netE.apply(weights_init)
        netE.load_state_dict(torch.load(net_E_file))


    netE.cuda()
    if args.out_path=='':
        process_folder(args.data_path, os.path.join(opt.outf, 'sequences'), opt, args, netE)
    else:
        os.makedirs(args.out_path,exist_ok=True)
        process_folder(args.data_path, args.out_path, opt, args, netE)






if __name__ == '__main__':
    main()
